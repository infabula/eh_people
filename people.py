from teacher import Teacher
from student import Student
from gender_exception import GenderException

try:
    student = Student("Joost", "Johansen", "Electronic Engineering")
    try:
        student.setGender(12)
    except GenderException as e:
        print("The gender value {} of the student needs to be changed".format(e.getGenderValue()))
        student.setGender(5)
    teacher = Teacher("Eva", "Storm")
    teacher.setGender(5)
    teacher.addCertificate("C/C++")
    teacher.addCertificate("Python for dummies")

    print(student.introduce())
    print(teacher.introduce())

    if student == teacher:
        print("The student and the teacher have the same name")
    else:
        print("The student and the teacher have different names")

except Exception as e:
    print("Exception: {}".format(str(e)))
