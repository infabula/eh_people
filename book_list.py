class BookList:
    def __init__(self):
        self.__booklist = []

    def add(self, title):
        pass

    def remove(self, title):
        # remote a title from the list
        pass
    
    def read(self, filename):
        # open the file and read the content

        # return the content as list
        pass
    
    def write(self, filename):
        # write the list to a file
        pass

    @property
    def booklist(self):
        return self.__booklist

if __name__ == "__main__":
    try:
        bl = BookList()
        bl.read("pythonbooks.txt")
        print(bl.booklist)
    except Exception as e:
        print("EXCEPTION: {}".format(e))
        
