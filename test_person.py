import unittest
from person import Person
from gender_exception import GenderException

class PersonTests(unittest.TestCase):
    def test_set_gender_with_negative_value(self):
        p = Person("Jan", "Pietersen")

        with self.assertRaises(GenderException):
            p.setGender(-1)

    def test_set_gender_with_valid_value(self):
        p = Person("Piet", "Pietersen")
        g = p.setGender(7)
        
        self.assertEqual(g, 7)
