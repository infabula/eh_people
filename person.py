from gender_exception import GenderException

class Person:
    # PROPERTIES
    forename = ''
    surname = ''
    date_of_birth = None
    __gender = None

    # METHODS
    def __init__(self, forename, surname, date_of_birth=None):
        self.forename = forename
        self.surname = surname
        self.date_of_birth = date_of_birth
        
    def introduce(self):
        return "My name is {} {} and I am {}".format(self.forename,
                                                    self.surname,
                                                    self.getGender())
        
    # setter
    def setGender(self, gender_value):
        if gender_value < 0 or gender_value > 10:
            raise GenderException(gender_value, "Invalid gender value!")
        else:
            self.__gender = gender_value
        return self.__gender

    # getter
    def getGender(self):
        if self.__gender >= 0 and self.__gender < 4:
            return "Very female"
        elif self.__gender >= 4 and self.__gender < 5:
            return "female"
        elif self.__gender >= 5 and self.__gender < 8:
            return "male"
        elif self.__gender >= 8 and self.__gender <= 10:
            return "very male"
    
    def __repr__(self):
        return self.forename + ' ' + self.surname

    
    def __eq__(self, other):
        if self.forename == other.forename:
            return True
        return False
        
