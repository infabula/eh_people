from datetime import datetime
from datetime import timedelta
from random import randint

def generateCheapEvent(datetime):
    while True:
        random_minutes = randint(5, 10)
        dt = timedelta(minutes=random_minutes)
        datetime += dt
        yield datetime

def generateExpensiveEventList(datetime, event_count):
    list = []
    for i in range(event_count):
        random_minutes = randint(5, 10)
        dt = timedelta(minutes=random_minutes)
        datetime += dt
        list.append(datetime)
    return list
        
counter = 0
nb_items = 10000
for event in generateCheapEvent(datetime.now()):
    if counter > nb_items:
        break
    print(event)
    counter += 1

print("expensive events:")
for event in generateExpensiveEventList(datetime.now(), nb_items):
    print(event)
