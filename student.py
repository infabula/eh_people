from person import Person
"""
module
"""
 
class Student(Person): 
    """
    Student class
    """
    study = None
    
    def __init__(self, forename, surname, study):
        super().__init__(forename, surname)
        self.study = study

    def introduce(self):
        intro_string = super().introduce()
        intro_string += "\nI am a student and I study {}".format(self.study)
        return intro_string


        
