class GenderException(Exception):
    # constructor
    def __init__(self, gender_value, msg=""):
        self.gender_value = gender_value
        self.msg = msg

        msg_string = "Gender value {} should be between 0 and 10".format(gender_value)
        
        super().__init__(msg_string)

    def getGenderValue(self):
        return self.gender_value
