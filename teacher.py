from person import Person

class Teacher(Person):
    certificats = []
    
    def __init__(self, forename, surname):
        super().__init__(forename, surname)

    def introduce(self):
        introString = super().introduce()
        introString = introString + "\nI am a teacher and I have the next certificates:\n"
        introString += self.getCertificats()
        return introString

    def addCertificate(self, certificate):
        self.certificats.append(certificate)

    def getCertificats(self):
        certificatString = ''
        # implenteer zelf de loop hier
        for c in self.certificats:
            certificatString += c + '\n'
        return certificatString
        
